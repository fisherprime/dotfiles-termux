;;
;; ~/.emacs.d/init.el
;;

;; REF: https://gist.github.com/nilsdeppe/7645c096d93b005458d97d6874a91ea9
;; REF: https://github.com/GuiltyDolphin/dotfiles/tree/master/dotfiles/emacs/custom

;; No frills
(when (display-graphic-p)
  (tool-bar-mode -1)   ; no tool bar with icons
  (scroll-bar-mode -1) ; no scroll bars
  )
(customize-set-variable 'inhibit-startup-screen t) ; No splash screen on start
(menu-bar-mode -1) ; no menu bar

;; Text wrap
(setq-default auto-fill-function 'do-auto-fill)
(setq-default fill-column 78)
(turn-on-auto-fill)
;; Disable auto-fill-mode in programming mode
(add-hook 'prog-mode-hook (lambda () (auto-fill-mode -1)))

;; Remove trailing white space upon saving
;; Note: because of a bug in EIN we only delete trailing whitespace
;; when not in EIN mode.
(add-hook 'before-save-hook
	  (lambda ()
	    (when (not (derived-mode-p 'ein:notebook-multilang-mode))
	      (delete-trailing-whitespace))))

;; Request y/n instead of yes/no
;; Additionally, SPC means yes & DEL means no
(defalias 'yes-or-no-p 'y-or-n-p)

;; Disable the horrid auto-save
(setq auto-save-default nil)
