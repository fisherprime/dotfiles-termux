#
# ~/.functions
#

# NOTE: Array indices start at 1 for most shells except `bash` & `ksh`
# starting at 0.
if [[ -n $BASH_VERSION ]]; then
	export ARRAY_START=0
else
	export ARRAY_START=1
fi

#
# Misc functions
# ----
man() {
	# Colourized man output
	env \
		LESS_TERMCAP_mb="$(printf "\e[1;31m")" \
		LESS_TERMCAP_md="$(printf "\e[1;31m")" \
		LESS_TERMCAP_me="$(printf "\e[0m")" \
		LESS_TERMCAP_se="$(printf "\e[0m")" \
		LESS_TERMCAP_so="$(printf "\e[1;44;33m")" \
		LESS_TERMCAP_ue="$(printf "\e[0m")" \
		LESS_TERMCAP_us="$(printf "\e[1;32m")" \
		man "$@"
} # End man

vim_update() {
	if [[ -x $(command -v vim) ]]; then
		printf '[*] Updating vim plugins\n'
		vim -c ":PlugUpgrade | :PlugUpdate | :CocUpdate | :qa!"
	fi
} # End vim_update

sys_update() {
	printf "[*] Updating termux\n"

	apt update &&
		apt full-upgrade --assume-yes &&
		apt autoremove --assume-yes

	vim_update
} # End sys_update
# ----

# Cowsay randomization
# ----
fortunecow() {
	# NOTE: Set this option to a positive integer >0: 72, 74, 80 if word
	# wrapping is desired.
	local wrap_column=0

	local cowfile=$RANDOM
	local cow_state=$RANDOM

	local cow_list
	local ignore_list

	ignore_list=("beavis.zen" "bong" "head-in" "hellokitty" "kiss" "satanic" "sodomized" "telebears" "three-eyes")

	_get_cow_list() {
		local list

		[[ -n $ZSH_VERSION ]] &&
			IFS=' ' read -Ar list <<<"$(cowsay -l | grep -v : | tr "\n" " ")"
		[[ -n $BASH_VERSION ]] &&
			IFS=' ' read -r -a list <<<"$(cowsay -l | grep -v : | tr "\n" " ")"

		for ignore in "${ignore_list[@]}"; do
			list=("${list[@]/$ignore/}")
		done

		for item in "${list[@]}"; do
			if [[ -n $item ]]; then
				[[ "${#cow_list[*]}" -eq 0 ]] && cow_list=("$item") && continue
				cow_list=("${cow_list[@]}" "$item")
			fi
		done
	}

	_get_cow_list

	[[ ${#cow_list[@]} -lt 1 ]] && return

	state_list=("-b" "-d" "-g" "-p" "-s" "-t" "-w" "-y")

	cowfile="${cow_list[$(((cowfile % ${#cow_list[*]}) + ARRAY_START))]}"
	cow_state="${state_list[$(((cow_state % ${#state_list[*]}) + ARRAY_START))]}"

	[[ $wrap_column -gt 0 ]] &&
		fortune | cowsay -W "$wrap_column" "$cow_state" -f "$cowfile" &&
		return "$SUCCESS_RET"

	fortune | cowsay -n "$cow_state" -f "$cowfile"

	return "$SUCCESS_RET"
}

# End fortunecow
# ----

# Network stuff
# ----
my_ip() {
	# Get current global IP

	printf '[*] Obtaining current global IP\n'
	curl -s checkip.dyndns.org | sed -E 's/.*Current IP Address: //; s/<.*$//'
} # End my_ip
# ----

# vim: ft=bash
