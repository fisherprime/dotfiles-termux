#
# ~/.exports
#

# Miscellaneous exports
# ----
export ROOT=/data/data/com.termux/files/

# export CC="gcc"
# export CXX="g++"

export EDITOR="vim"
export IGNOREEOF=50
export PAGER="less"
export PYTHON="python"
export VISUAL="vim"
# ----

# PATH exports
# ----
appendpath() {
	case ":$PATH:" in
	*:"$1":*) ;;

	*)
		PATH="${PATH:+$PATH:}$1"
		;;
	esac
}

export CARGO_HOME="$HOME/.cargo"
[[ ! -d "$CARGO_HOME" ]] && mkdir -p "$CARGO_HOME"

appendpath "$HOME/.local/bin"
appendpath "$CARGO_HOME/bin"
# ----

# Compiler flags
# ----
# "-pedantic" Issue all the warnings demanded by strict ISO C and ISO C++
# 	Reject all programs that use forbidden extensions, and some other
# 	programs that do not follow ISO C and ISO C++
# 	Follows "-std" option if specified
# ISO C99 Standard headers do not define identifiers that the C standard
# does not explicitly declare
# Using GNU C extensions that violate ISO C11 standards

# Using CMake to handle project libraries and linker stuff
#export LINKERFLAGS=( "-I/usr/include/tirpc" "-lnsl" "-ltirpc" "-lsctp" )

# Don't use "-pipe" on systems with limited amounts of memory <=4GB to
# compile large packages

# Alternative linkers: -fuse-ld=gold, -fuse-ld=lld
export GENLIBS=("-lpthread" "-lm")

export GENFLAGS=("-fstack-protector" "-Wall" "-Wextra" "-pedantic"
	"-Wfloat-equal" "-ftrapv" "-Wvla" "-O2" "-fuse-ld=gold" "-pipe")
export GENFLAGS_STRICT=("${GENFLAGS[*]}" "-fsanitize=address"
	"-fsanitize=pointer-compare" "-fsanitize=pointer-subtract")

export DEBUG_ARRAY=("-Wall" "-Wextra" "-pedantic" "-Wfloat-equal" "-ftrapv"
	"-Wvla" "-ggdb" "-pipe" "-fopt-info")

export CFLAGS_ARRAY=("-std=gnu18" "-Wstrict-prototypes" "${GENFLAGS[*]}")
export CXXFLAGS_ARRAY=("-std=gnu++17" "${GENFLAGS[*]}") # -lboost_system

export CFLAGS_STRICT_ARRAY=("-std=gnu11" "-Wstrict-prototypes"
	"${GENFLAGS_STRICT[*]}")
export CXXFLAGS_STRICT_ARRAY=("-std=gnu++14"
	"${GENFLAGS_STRICT[*]}") # -lboost_system

# Since one can't use arrays as environment variables
export CFLAGS=${CFLAGS_ARRAY[*]}
export CXXFLAGS=${CXXFLAGS_ARRAY[*]}
export CFLAGS_STRICT=${CFLAGS_STRICT_ARRAY[*]}
export CXXFLAGS_STRICT=${CXXFLAGS_STRICT_ARRAY[*]}
export DEBUG_FLAGS=${DEBUG_ARRAY[*]}

export MAKEFLAGS
MAKEFLAGS="-j$(nproc)"
# ----

if [[ -n $BASH_VERSION ]]; then
	export PS1="Live> "
	export PS2="Hmmm...> "
	export PS4="Debug> "
fi

# nnn stuff
# ----
if [[ -x "$(command -v nnn)" ]]; then
	[[ -x "$(command -v pistol)" ]] && export USE_PISTOL=1

	export GUI=1
	export NNN_FIFO="$ROOT/usr/tmp/nnn.fifo"
	export NNN_OPTS="daExc"
	export NNN_USE_EDITOR=1

	export NNN_PLUG_MEDIA='m:-mediainf $nnn;v:-preview-tui'
	export NNN_PLUG_MODE='w:-_chmod +w $nnn*;W:-_chmod -w $nnn*'
	export NNN_PLUG='g:-_git log --all --graph*;p:-_less -iR $nnn*;d:diffs;'"$NNN_PLUG_MODE;$NNN_PLUG_MEDIA"

	# REF: https://github.com/jarun/nnn/wiki/Usage#program-options.
	[[ -x "$(command -v atool)" || -x "$(command -v bsdtar)" ]] &&
		export NNN_ARCHIVE="\\.(7z|a|ace|alz|arc|arj|bz|bz2|cab|cpio|deb|gz|jar|lha|lz|lzh|lzma|lzo|rar|rpm|rz|t7z|tar|tbz|tbz2|tgz|tlz|txz|tZ|tzo|war|xpi|xz|Z|zip|zst)$"
fi
# ----

# vim: ft=sh
