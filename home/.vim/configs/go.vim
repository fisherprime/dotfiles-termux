"
" ~/.vim/configs/go.vim
"

" Run GoMetaLinter on save.
"let g:go_metalinter_autosave = 1

" Specify the command to run on `:GoMetaLinter`.
let g:go_metalinter_command = 'golangci-lint'

" let g:go_metalinter_enabled = []

let g:go_metalinter_deadline = '30s'
