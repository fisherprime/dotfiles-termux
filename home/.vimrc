"
" ~/.vimrc
"

" NOTE
" ----
" <Leader> is '\'
" Vim doesn't show the EOL at EOF, but it is there.
" Some lifting from archlinux.vim.
" ----

" Flags
" ----
" Disable Vi backward compatibility,
" This option is set when either: '.vimrc', '.gvimrc' " file(s) are sourced.
" if &compatible
" set nocompatible
" endif

" Make backspace work as other programs, not necessary.
" set backspace=2

if has('termguicolors')
  " The assignments below resolve monochrome dispays with urxvt & Alacritty.
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

  set termguicolors
endif

" set signcolumn=number " Display signs in the 'number' column.
set autoindent
set autoread
set background=dark
set cmdheight=2 " Allow for more message space.
set confirm
set diffopt+=algorithm:patience " Use the slower patience algorithm for diffing.
set display=lastline " Display as much of the last line as possible.
set encoding=utf8
set guifont=DejaVuSansMono\ Nerd\ Font\ Mono\ 12 " Font used in the GUI version of vim.
set history=1000
set incsearch " Incremental search.
set laststatus=2
set modeline " Unset for privileged vim.
set noexpandtab " Do not replace tabs with spaces.
set number
set shiftwidth=4 " Set indent to desired <Tab> width.
set smartindent
set softtabstop=0 " Number of spaces that a <Tab> counts for while editing, mixes <Tabs> & spaces.
set spell spelllang=en_gb " Enable spell checking.
set synmaxcol=500 " Avoid slow redraw for long lines.
set tabstop=4 " Number of spaces a <Tab> in the file counts for.
set undofile
set undolevels=10000
set undoreload=10000
set wildmenu " Command completion.
set wildmode=full

" Use ripgrep as the grep program if it exists.
if executable('rg') == 1
  set grepprg=rg\ --vimgrep\ --color\ always\ $*
endif

scriptencoding utf-8

" Word wrapping
set linebreak " Only wrap at character in the break at option.
set nolist  " list disables line break.
set textwidth=100
set wrap

" Persistent undo file directory.
" if has('persistent_undo')
" set undodir=~/.undodir/
" set undofile
" endif

" Enable mouse usage.
" a       all previous modes
" c       Command-line mode
" h       all previous modes when editing a help file
" i       Insert mode
" n       Normal mode and Terminal modes
" r       for hit-enter and more-prompt prompt
" v       Visual mode
if has('mouse')
  set mouse=a
endif
" ----

" Environment variables
" ----
" Rust debugging
let $RUST_BACKTRACE='full'
" ----

" ----
" Move temporary files to a secure location to protect against
" CVE-2017-1000382.
if exists('$XDG_CACHE_HOME')
  let &g:directory=$XDG_CACHE_HOME
else
  let &g:directory=$HOME . '/.cache'
endif

let &g:undodir=&g:directory . '/vim/undo/'
let &g:backupdir=&g:directory . '/vim/backup/'
let &g:directory.='/vim/swap/'

" Create directories if they doesn't exist.
if ! isdirectory(expand(&g:directory))
  silent! call mkdir(expand(&g:directory), 'p', 0700)
endif
if ! isdirectory(expand(&g:backupdir))
  silent! call mkdir(expand(&g:backupdir), 'p', 0700)
endif
if ! isdirectory(expand(&g:undodir))
  silent! call mkdir(expand(&g:undodir), 'p', 0700)
endif
" ----

" vim plugins
" ----
" Load plugins for specific file types, needed by nerdCommenter & use file type
" specific indentation.
filetype plugin indent on

" Using 'syntax on' overrides the colours with Vim's syntax defaults.
syntax enable

" Load matchit.vim; extended % matching.
" The '!' instructs to source all matches, useful for globbing.
runtime! macros/matchit.vim

" Load optional vim plugins.
packadd termdebug
" ----

" vim-plug plugins
" ----

" Autoinstall vim-plug.
augroup MySetup
  if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
          \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif
augroup END

" REF: https://github.com/junegunn/vim-plug#post-update-hooks.
function SetupVimGo(info)
  " info is a dictionary with 3 fields
  " - name:   name of the plugin
  " - status: 'installed', 'updated', or 'unchanged'
  " - force:  set on PlugInstall! or PlugUpdate!
  if a:info.status ==# 'installed' || a:info.force
    " 1st install will always fail, plugin not yet loaded.
    :GoInstallBinaries
  elseif a:info.status ==# 'updated'
    :GoUpdateBinaries
  endif
endfunction

" function SetupGoTests(info)
  " if a:info.status ==# 'unchanged' || a:info.force
    " return
  " endif
"
  " !go install github.com/cweill/gotests/...@latest
" endfunction

function SetupCoc(info)
  if a:info.status ==# 'updated'
    :CocUpdate
    :CocRebuild
  endif
endfunction

" There is no need for setting on-demand loading for some language specific plugins.
"
" Minor impact to startup time.
call plug#begin('~/.vim/plugins')

" Plug 'metakirby5/codi.vim' " Interactive scratch pad for hackers.
Plug 'universal-ctags/ctags'
Plug 'vim-scripts/DoxygenToolkit.vim' " Doxygen support.

" Language
" Plug 'jimhester/lintr', {'for': 'r'} " Static code analysis for r.
Plug 'fatih/vim-go', {'do': function('SetupVimGo')} " | Plug 'buoto/gotests-vim', {'do': function('SetupGoTests')}
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown' " Tabular is a vim-markdown dependency.
Plug 'lervag/vimtex' " Latex support.
Plug 'nfnty/vim-nftables' " Nftables syntax highlighting.
Plug 'rust-lang/rust.vim', {'for': 'rust'}
Plug 'scrooloose/nerdcommenter' " Orgasmic commenting.
Plug 'sheerun/vim-polyglot' " A solid language pack for vim.
Plug 'tamarin-prover/editors'
Plug 'udalov/kotlin-vim', {'for': 'kotlin'}

" Completion
Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets' " Snippets engine & actual snippets.
Plug 'chrisbra/unicode.vim', {'do': ':UnicodeDownload'} " Unicode glyph completion.
Plug 'mattn/emmet-vim' " Emmet for vim; essential toolkit for webdevs.
Plug 'neoclide/coc.nvim', {'branch': 'release', 'do': function('SetupCoc')} " Better language server completions, made with NodeJS.
Plug 'raimondi/delimitmate' " Close: quotes, parens, brackets.

" Code display
" Plug 'scrooloose/nerdtree', {'on': 'NERDTreeToggle'} " Directory tree, loaded on demand.
Plug 'dense-analysis/ale' " Asynchronous Lint Engine.
Plug 'sainnhe/sonokai' " Colourschem.
Plug 'tpope/vim-surround' " Brace management.

" Integrations
" Plug 'shumphrey/fugitive-gitlab.vim' " Gitlab integration for fugitive.
Plug 'puremourning/vimspector' " A multi-language debugging system for vim.
Plug 'tpope/vim-fugitive' " Git management.
Plug '~/.vim/local-plugins/gtags'

" Interface
Plug 'airblade/vim-gitgutter' " Show git diff in the 'gutter'.
Plug 'ctrlpvim/ctrlp.vim' " Fuzzy for files, buffers, mru.
Plug 'junegunn/limelight.vim' | Plug 'junegunn/goyo.vim' " Distraction free writing.
Plug 'majutsushi/tagbar' " Display tags in a window.
Plug 'mbbill/undotree' " Undo history visualizer.
Plug 'ryanoasis/vim-devicons'
Plug 'vim-airline/vim-airline' | Plug 'vim-airline/vim-airline-themes' " Statusbar & themes.
Plug 'yuttie/comfortable-motion.vim' " Physics-based smooth scrolling.

" Commands
Plug 'andrewradev/splitjoin.vim' " Switch between single-line & multiline forms of code
Plug 'junegunn/vim-easy-align' " Alignment plugin.
Plug 'tpope/vim-dadbod' | Plug 'kristijanhusak/vim-dadbod-ui' " Modern database interface for vim & a UI for it.
Plug 'tpope/vim-obsession' " Continuously update session files.
Plug 'tpope/vim-repeat' " Repeat non-native vim commands.
Plug 'tpope/vim-speeddating' " Time adjustment.

" Misc
Plug 'andrewradev/tagalong.vim' " Rename accompanying html tag.
Plug 'normen/vim-pio' " PlatformIO support.

" Initialize plugin system.
call plug#end()
" ----

" Functions
" ----
" Function names either start with a capital letter or 's:'
" fu | fu! | function | function!
" The '!' means you want to replace an existing function of the same name

" function StripSpaces()
" %s/\s\+$//e
" endfunction

" Update installed plugins & CoC extensions.
function MyUpdateAll()
  if !empty(glob('~/.vim/autoload/plug.vim'))
    :PlugUpgrade
    :PlugUpdate
  endif

  if !empty(glob('~/.vim/plugins/coc.nvim'))
    :CocUpdate
  endif

  :qa!
endfunction
" ----

" Git gutter config
" ----
let g:gitgutter_grep=''
" ----

" NerdCommenter config
" ----
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Allow commenting and inverting empty lines (useful when commenting a region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing white space when uncommenting
let g:NERDTrimTrailingWhitespace = 1
" ----

" NerdTree config
" ----
" source ~/.vim/configs/nerdtree.vim
" ----

" Netrw config
" ----
" Built-in replacement for NerdTree
let g:netrw_banner = 0
let g:netrw_winsize = 27
let g:netrw_liststyle = 3 " Tree style listing

" Open netrw when no file is specified
function NetrwStartup()
  if argc() == 0 && !exists('s:std_in')
    Lexplore
  endif
endfunction

augroup MyNetrw
  autocmd VimEnter * call NetrwStartup()
augroup END
" ----

" Airline config
" ----
let g:airline#extensions#tabline#enabled = 1 " Use airline's tabline
let g:airline#extensions#tabline#formatter = 'unique_tail_improved'

let g:airline_left_alt_sep = '|'
let g:airline_left_sep = ' '
let g:airline_right_alt_sep = '|'
let g:airline_right_sep = ' '

" Set Airline's theme
let g:airline_theme = 'deus'
" let g:airline_theme = 'sonokai'
" ----

" ALE  config
" ----
let g:ale_echo_msg_format = '[%linter%] %code: %%s'

let g:ale_fixers = {
      \ '*': ['remove_trailing_lines', 'trim_whitespace'],
      \ 'c' : ['clangtidy', 'clang-format'],
      \ 'cmake': ['remove_trailing_lines', 'trim_whitespace', 'cmakeformat'],
      \ 'cpp' : ['clangtidy','clang-format'],
      \ 'cs' : ['clangtidy', 'clang-format'],
      \ 'css': ['prettier', 'stylelint'],
      \ 'go' : ['goimports', 'gofmt'],
      \ 'html': ['prettier'],
      \ 'java' : ['google_java_format'],
      \ 'javascript': ['prettier', 'eslint'],
      \ 'json': ['prettier','jq'],
      \ 'jsonc': ['prettier','jq'],
      \ 'kotlin': ['ktlint'],
      \ 'markdown': ['prettier'],
      \ 'perl': ['remove_trailing_lines', 'trim_whitespace', 'perltidy'],
      \ 'plaintex': ['remove_trailing_lines', 'trim_whitespace', 'latexindent'],
      \ 'python': ['isort', 'autopep8'],
      \ 'ruby': ['remove_trailing_lines', 'trim_whitespace', 'rubocop'],
      \ 'rust': ['rustfmt'],
      \ 'scss': ['prettier', 'stylelint'],
      \ 'sh': ['remove_trailing_lines', 'trim_whitespace', 'shfmt'],
      \ 'sql': ['pgformatter'],
      \ 'tex': ['remove_trailing_lines', 'trim_whitespace', 'latexindent'],
      \ 'text': ['remove_trailing_lines', 'trim_whitespace'],
      \ 'typescript': ['prettier', 'eslint'],
      \ 'vue': ['prettier', 'eslint'],
      \ 'yaml': ['prettier'],
      \ 'yaml.docker-compose': ['prettier'],
      \ 'zsh': ['remove_trailing_lines', 'trim_whitespace', 'shfmt']
      \ }

" Linter aliases allow for the running of arbitrary file type linters for any
" file type.
let g:ale_linter_aliases = {'vue': [ 'vue', 'javascript', 'scss', 'css' ]}

let g:ale_fix_on_save = 0
" Open preview-window when cursor is over problematic lines
let g:ale_cursor_detail = 1

let g:ale_c_flawfinder_error_severity=1

let g:ale_cpp_clang_options = '-std=gnu++17 -Wall -Wextra -Wpedantic'

"" Resolve errors showing up in the `.kt` source file(s) on `ALEFix`.
" let g:ale_kotlin_ktlint_options = '1>&2'
" ----

" CoC config
" ----
source ~/.vim/configs/coc.vim
" ----

" Vim-go config
" ----
source ~/.vim/configs/go.vim
" ----

" Ultisnips config
" ----
" Trigger configuration. Do not use <tab> if you use https://github.com/Valloric/YouCompleteMe.
let g:UltiSnipsExpandTrigger = '<C-t>'
let g:UltiSnipsJumpBackwardTrigger = '<C-z>'
let g:UltiSnipsJumpForwardTrigger = '<C-b>'
let g:UltiSnipsUsePythonVersion = 3
" ----

" Doxygen config
" ----
let g:doxygen_enhanced_colour = 1 " Use non-standard highlighting for doxygen comments
let g:load_doxygen_syntax = 1 " Enable doxygen formatting for supported file types

" let g:DoxygenToolkit_blockFooter=''
" let g:DoxygenToolkit_blockHeader=''
" let g:DoxygenToolkit_briefTag_pre='@brief  '
" let g:DoxygenToolkit_licenseTag='My own license' " !!! Does not end with '\<enter>'
" let g:DoxygenToolkit_paramTag_pre='@param '
" let g:DoxygenToolkit_returnTag='@return   '
let g:DoxygenToolkit_authorName = 'fisherprime'
let g:DoxygenToolkit_commentType = 'C' " C++'s uses ///
" ----

" Tagbar config
" ----
source ~/.vim/configs/tagbar.vim
" ----

" Comfortable motion config
" ----
let g:comfortable_motion_friction = 100.0 " 80.0
let g:comfortable_motion_air_drag = 1.2 " 2.0

" Scrolling proportional to window size
" let g:comfortable_motion_no_default_key_mappings = 1
" let g:comfortable_motion_impulse_multiplier = 1  " Feel free to increase/decrease this value.
" nnoremap <silent> <C-d> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 2)<CR>
" nnoremap <silent> <C-u> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -2)<CR>
" nnoremap <silent> <C-f> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * 4)<CR>
" nnoremap <silent> <C-b> :call comfortable_motion#flick(g:comfortable_motion_impulse_multiplier * winheight(0) * -4)<CR>
" ----

" Undotree config
" ----
let g:undotree_SetFocusWhenToggle = 1
let g:undotree_TreeNodeShape = 'o'
let g:undotree_WindowLayout = 2
" ----

" Vim-markdown config
" ----
let g:vim_markdown_autowrite = 1 " Save before following a link with 'ge'.
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
let g:vim_markdown_math = 1
let g:vim_markdown_strikethrough = 1
let g:vim_markdown_toml_frontmatter = 1
" ----

" Vimtex config
" ----
let g:tex_flavor = 'latex'

if executable('tectonic') == 1
  let g:vimtex_compiler_method = 'tectonic'
else
  let g:vimtex_compiler_method = 'latexmk'
endif
" ----

" Omnisharp config
" ----
let g:OmniSharp_server_use_mono = 1
" ----

" Keymaps
" ----
" map Map a key sequence to {rhs}
" imap Map for insert mode
" nmap Map for normal mode
" vmap Map for visual mode
" noremap Map but don't map {rhs}, avoids nested mappings

" Toggle the `undotree`.
nnoremap <silent> <F5> :UndotreeToggle<cr>

" Toggle the `tagbar`.
nnoremap <silent> <F9> :TagbarToggle<CR> " Toggle Tagbar

" Toggle goyo.
noremap <silent> <Leader>goyo :Goyo<CR>

" Toggle limelight.
noremap <silent> <Leader>l :Limelight!!<CR>

" Toggle DadbodUI.
noremap <silent> <Leader>db :DBUIToggle<CR>

" Start EasyAlign.
vmap <silent> <Leader><Enter> <Plug>(LiveEasyAlign)

" Timestamp.
" See `man(3)strftime` for format specifiers.
nnoremap <silent> <Leader>date :r! date<CR>
nnoremap <silent> <Leader>datt i<C-R>=strftime("%FT%T%z")<CR><Esc>

" Insert today's date (ISO formatted).
nnoremap <silent> <Leader>now "=strftime('%Y-%m-%d')<C-M>p

" Navigation.
noremap <silent> [b :bprevious<CR>
noremap <silent> [t :tabprevious<CR>
noremap <silent> ]b :bnext<CR>
noremap <silent> ]t :tabnext<CR>
" noremap <silent> <Leader>n :NERDTreeToggle<CR> " Toggle NerdTree
noremap <silent> <Leader>n :Lexplore<CR> " Toggle Netrw
noremap <silent> <Leader>tn :tabnew<CR>

" Linting.
" nmap <Leader>ss :call StripSpaces()<CR>
map <silent> <F7> <Plug>(ale_lint)
map <silent> <F8> <Plug>(ale_fix)
nmap <silent> [a <Plug>(ale_previous_wrap)
nmap <silent> ]a <Plug>(ale_next_wrap)

" Disable CTRL-A on 'tmux' or 'screen'
if $TERM =~# 'screen'
  nnoremap <C-a> <nop>
  nnoremap <Leader><C-a> <C-a>
endif

" Quit
" inoremap <C-Q>     <esc>:q<cr>
" nnoremap <C-Q>     :q<cr>
" vnoremap <C-Q>     <esc>
" nnoremap <Leader>q :q<cr>
" nnoremap <Leader>Q :qa!<cr>
" ----

" Language-specific configs
" ----
" Java
" let java_highlight_debug=1
" ----

" Eye candy
" ----
" let g:sonokai_style = 'shusia'
let g:sonokai_better_performance = 1

colorscheme sonokai

let g:limelight_conceal_ctermfg = 'DarkGrey' " Colour for dimming surrounding paragraphs
let g:limelight_paragraph_span = 1 " Preceding & following paragraphs to include

" Goyo + Limelight integration
augroup MyGoyo
  autocmd! User GoyoEnter Limelight
  autocmd! User GoyoLeave Limelight!
augroup END
" ----

" ALE stuff
" ----
" This will mess things up if you use plugin manager.

" Load all plugins at this point.
" Plugins need to be added to runtimepath before helptags can be generated.
" packloadall

" Load all of the helptags now, after plugins have been loaded.
" All messages and errors will be ignored.
" silent! helptags ALL
" ----

" Dadbod
" ----
let g:db_ui_use_nerd_fonts = 1
let g:db_ui_show_database_icon = 1
" let g:db_ui_table_helpers = {
" \  'postgresql': {
" \    'List': 'select * from "{table}" limit 10 order by id asc'
" \  }
" \}
" ----

" Misc autocommands
" ----
augroup MyMisc
  " Use proper git commit message wrapping.
  autocmd Filetype gitcommit setlocal spell textwidth=72
augroup END
" ----
